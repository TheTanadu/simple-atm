document.addEventListener("DOMContentLoaded", function () {
    function final() {
        let box = document.querySelector(".container");

        function render() {
            // Make buttons
            let resetButton = document.createElement("button");
            resetButton.className += "resetButton";
            resetButton.innerHTML = "Reset";
            box.appendChild(resetButton);
            let startButton = document.createElement("button");
            startButton.className += "checkButton";
            startButton.innerHTML = "Check";
            box.appendChild(startButton);
            // Make digits boxes
            for (let i = 0; i < 4; i++) {
                let underScore = document.createElement("span");
                underScore.className = "digit";
                underScore.innerHTML = " ";
                box.appendChild(underScore);
            }
        }

        function addDigits() {
            //Write digits in boxes
            let digits = box.querySelectorAll(".digit");
            let counter = 0;
            document.addEventListener("keydown", function (event) {
                if (counter != 4 && event.key.match(/[0-9]/)) {
                    digits[counter].innerHTML = event.key;
                    counter++;
                }
            });
            box.querySelector(".resetButton").addEventListener("click", function () {
                let digits = box.querySelectorAll(".digit")
                for (let i = 0; i < 4; i++) {
                    digits[i].innerHTML = " ";
                }
                addDigits();
            });
        }

        function check() {
            let data = ["3458", "7629"];
            let digits = box.querySelectorAll(".digit");
            let number = [];
            for (let i = 0; i < 4; i++) {
                number.push(digits[i].innerText);
            }
            number = number.join("");
            let isThere = (data.indexOf(number) > -1);
            if (isThere) {
                alert("You pass");
            } else {
                alert("You didn't pass");
            }
            box.querySelector(".resetButton").addEventListener("click", function () {
                let digits = box.querySelectorAll(".digit")
                for (let i = 0; i < 4; i++) {
                    digits[i].innerHTML = " ";
                }
                addDigits();
            });
        }

        render();
        addDigits();
        box.querySelector(".checkButton").addEventListener("click", check);
    }

    final();
});